#pragma once

#define GSS_ESP_NAME            "dwrsp.esp"
#define UC_ESP_NAME            "UltimateCombat.esp"
#define GSS_PAPYRUS_CLASS_NAME  "GunsmithSystem"
#define GSS_KEYWORD_FORMID   0x0484D9

class VMClassRegistry;
struct StaticFunctionTag;

extern bool GSS_Active;
bool BlockAnimGSS(std::string str);

namespace PapyrusGunsmithSystem
{ 
	void RegisterFuncs(VMClassRegistry* registry);
}

namespace EventGunsmithSystem
{
	void Init(void);
}
