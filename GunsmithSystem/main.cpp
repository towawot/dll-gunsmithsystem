#include <skse.h>
#include <skse/PluginAPI.h>
#include <skse/PapyrusVM.h>
#include <skse/GameEvents.h>
#include <skse/GameData.h>
#include <shlobj.h>

#include "GunsmithSystem.h"
#include "events.h"

IDebugLog gLog;

PluginHandle g_pluginHandle = kPluginHandle_Invalid;


DECLARE_EVENT_HANDLER(MagicEffectApply, MagicEffectApplyEventHandler);
EventResult MagicEffectApplyEventHandler::ReceiveEvent(TESMagicEffectApplyEvent *evn, DispatcherT *dispatcher)
{
	DataHandler* dhnd = DataHandler::GetSingleton();

	if (dhnd->LookupModByName(UC_ESP_NAME) != NULL)
	{
		_MESSAGE("warning: detect UC!! stop loading");
	}
	else if (dhnd->LookupModByName(GSS_ESP_NAME) != NULL)
	{
		PapyrusGunsmithSystem::RegisterFuncs((*g_skyrimVM)->GetClassRegistry()); 
		_MESSAGE("registers PapyrusGunsmithSystem");

		EventGunsmithSystem::Init();
		_MESSAGE("registers EventGunsmithSystem");

		PapyrusEvent::Init(); 
		_MESSAGE("registers events");

	}
	else
	{
		_MESSAGE(GSS_ESP_NAME " is not active.");
	}

	Unregister();
	return kEvent_Continue;
}

//=====================================

extern "C"
{

bool SKSEPlugin_Query(const SKSEInterface * skse, PluginInfo * info)
{
	gLog.OpenRelative(CSIDL_MYDOCUMENTS, "\\My Games\\Skyrim\\SKSE\\skse_GunsmithSystem.log");

	// populate info structure
	info->infoVersion =	PluginInfo::kInfoVersion;
	info->name =		"ultimate combat plugin";
	info->version =		1;
	
	// store plugin handle so we can identify ourselves later
	g_pluginHandle = skse->GetPluginHandle();
	
	if(skse->isEditor)
	{
		_MESSAGE("loaded in editor, marking as incompatible");
		return false;
	}

	// ランタイムバージョンを解釈
	int major = (skse->runtimeVersion >> 24) & 0xFF;
	int minor = (skse->runtimeVersion >> 16) & 0xFF;
	int build = (skse->runtimeVersion >> 8) & 0xFF;
	int sub   = skse->runtimeVersion & 0xFF;

	if(skse->runtimeVersion != SKSE_SUPPORTING_RUNTIME_VERSION)
	{
		_MESSAGE("unsupported runtime version 1.%d.%d.%d", major, minor, build);

		major = (SKSE_SUPPORTING_RUNTIME_VERSION >> 24) & 0xFF;
		minor = (SKSE_SUPPORTING_RUNTIME_VERSION >> 16) & 0xFF;
		build = (SKSE_SUPPORTING_RUNTIME_VERSION >> 8) & 0xFF;
		sub   = SKSE_SUPPORTING_RUNTIME_VERSION & 0xFF;
		_MESSAGE("(this plugin needs 1.%d.%d.%d)", major, minor, build);
		return false;
	}

	_MESSAGE("skyrim runtime version: 1.%d.%d.%d ... OK !", major, minor, build);
	return true;
}


bool SKSEPlugin_Load(const SKSEInterface * skse)
{
	_MESSAGE("loaded (handle:%08X)", g_pluginHandle);

	static MagicEffectApplyEventHandler s_handlerMagicEffect;
	s_handlerMagicEffect.Register();

	return true;
}

}
