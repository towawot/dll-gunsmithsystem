#include <skse.h>
#include <skse/GameReferences.h>
#include <skse/GameForms.h>
#include <skse/GameObjects.h>
#include <skse/GameData.h>
#include <skse/GamePapyrus.h>
#include <skse/PapyrusVM.h>
#include <skse/PapyrusNativeFunctions.h>
#include <skse/GameRTTI.h>

#include "GunsmithSystem.h"
#include <string>
#include <algorithm>

bool GSS_Active;
bool GSS_bBlockAttackStart;
bool GSS_bBlockAttackRelease;
bool GSS_bBlockReloadStart;
UInt32 g_GSSKeyword;

bool HasKeyword(TESForm* pForm, UInt32 Key)
{
	if (pForm)
	{
		BGSKeywordForm* keywordForm = DYNAMIC_CAST(pForm, TESForm, BGSKeywordForm);

		if (Key)
		{
			UInt32 count = keywordForm->numKeywords;
			BGSKeyword ** keywords = keywordForm->keywords;
			BGSKeyword* keyword = (BGSKeyword*)LookupFormByID(Key);

			if(keywords)
			{
				for(int i = 0; i < count; i++)
				{
					BGSKeyword* pKey = keywords[i];
					if(pKey)
					{
						if (pKey == keyword)
									return true;
					}
				}
			}
		}
	}
	return false;
}

bool BlockAnimGSS(std::string str)
{
	PlayerCharacter* player = *g_thePlayer;
	std::transform(str.begin(), str.end(), str.begin(), ::tolower);
	
	if (str == "attackrelease" || str == "crossbowattackstart" || str == "attackstart" || str == "reloadstart")
	{
		TESForm* form = player->GetEquippedObject(0);
		if (form)
		{
			if (HasKeyword(form, g_GSSKeyword))
			{
				if (str == "attackrelease" && GSS_bBlockAttackRelease)
				{
					return true;
				}
				else if ((str == "crossbowattackstart" || str == "attackstart") && GSS_bBlockAttackStart)
				{
					return true;
				}
				else if (str == "reloadstart" && GSS_bBlockReloadStart)
				{
					return true;
				}
			}
		}
	}
	return false;
}

// 追加パピルス関数
namespace PapyrusGunsmithSystem
{
	static const char s_className[] = GSS_PAPYRUS_CLASS_NAME;

	void BlockAttackRelease(StaticFunctionTag*, bool flag)
	{
		GSS_bBlockAttackRelease = flag;
	}
	
	void BlockAttackStart(StaticFunctionTag*, bool flag)
	{
		GSS_bBlockAttackStart = flag;
	}

	void BlockReloadStart(StaticFunctionTag*, bool flag)
	{
		GSS_bBlockReloadStart = flag;
	}

	bool IsBlockAttackRelease(StaticFunctionTag*)
	{
		return GSS_bBlockAttackRelease;
	}
	
	bool IsBlockAttackStart(StaticFunctionTag*)
	{
		return GSS_bBlockAttackStart;
	}

	bool IsBlockReloadStart(StaticFunctionTag*)
	{
		return GSS_bBlockReloadStart;
	}
	
	// パピルス関数を登録するときに呼ばれる
	void RegisterFuncs(VMClassRegistry* registry)
	{
	registry->RegisterFunction(
		new NativeFunction1<StaticFunctionTag, void, bool>("BlockAttackRelease", s_className, BlockAttackRelease, registry));
	registry->RegisterFunction(
		new NativeFunction1<StaticFunctionTag, void, bool>("BlockAttackStart", s_className, BlockAttackStart, registry));
	registry->RegisterFunction(
		new NativeFunction1<StaticFunctionTag, void, bool>("BlockReloadStart", s_className, BlockReloadStart, registry));
	registry->RegisterFunction(
		new NativeFunction0<StaticFunctionTag, bool>("IsBlockAttackRelease", s_className, IsBlockAttackRelease, registry));
	registry->RegisterFunction(
		new NativeFunction0<StaticFunctionTag, bool>("IsBlockAttackStart", s_className, IsBlockAttackStart, registry));
	registry->RegisterFunction(
		new NativeFunction0<StaticFunctionTag, bool>("IsBlockReloadStart", s_className, IsBlockReloadStart, registry));
	}
}

namespace EventGunsmithSystem
{
	void Init(void)
	{
		DataHandler* dhnd = DataHandler::GetSingleton();
		const ModInfo* GSSInfo = dhnd->LookupModByName(GSS_ESP_NAME);
		if (GSSInfo)
		{
			UInt8 g_GSSIndex = GSSInfo->modIndex;
			g_GSSKeyword = (g_GSSIndex << 24) | GSS_KEYWORD_FORMID;

			GSS_Active = true;

			#ifdef _DEBUG
				if (g_GSSKeyword)
				{
					_MESSAGE("g_GSSKeyword %08X", g_GSSKeyword);
				}
			#endif
		}
	}
}
