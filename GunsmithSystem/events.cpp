#include <skse.h>
#include <skse/GameData.h>
#include <skse/GameReferences.h>
#include <skse/GameEvents.h>
#include <skse/GameForms.h>
#include <skse/GamePapyrus.h>
#include <skse/GameRTTI.h>
#include <skse/SafeWrite.h>

#include "GunsmithSystem.h"
#include "events.h"
#include "HookUtil.h"


#if SKSE_VERSION_RELEASEIDX >= 45
// RUNTIME_VERSION_1_9_32_0
#define ADDR_IAnimationGraphManagerHolder_VPTR					0x010D166C
#define ADDR_IPlayerAnimationGraphManagerHolder_VPTR			0x010D1FB4
#elif SKSE_VERSION_RELEASEIDX == 39
// RUNTIME_VERSION_1_9_29_0
#error 対応していないバージョンです。
#elif SKSE_VERSION_RELEASEIDX == 38
#error 対応していないバージョンです。
#elif SKSE_VERSION_RELEASEIDX >= 31
// RUNTIME_VERSION_1_8_151_0
#define ADDR_IAnimationGraphManagerHolder_VPTR					0x010D1024
#define ADDR_IPlayerAnimationGraphManagerHolder_VPTR			0x010D196C
#else
#error 対応していないバージョンです。
#endif


#ifdef _DEBUG
// アクターの名前を取得
static const char* GetActorName(Actor* akActor)
{
	static const char unkName[] = "unknown";
	const char* result = unkName;

	if (akActor && akActor->formType == kFormType_Character)
	{
		TESActorBase* actorBase = (TESActorBase*)akActor->baseForm;
		if (actorBase)
		{
			TESFullName* pFullName = DYNAMIC_CAST(actorBase, TESActorBase, TESFullName);
			
			if (pFullName)
			{
				result = pFullName->name.data;
			}
		}
	}

	return result;
}
#endif


static bool Log_IAnimationGraphManagerHolder_SendEvent(UInt32* stack, UInt32 ecx)
{
	BSFixedString* animName = (BSFixedString*)stack[1];
	TESObjectREFR* ref = (TESObjectREFR*)(ecx - 0x20);

	if (ref->formType == kFormType_Character)
	{
#ifdef _DEBUG
		const char* name = GetActorName((Actor*)ref);
#endif
		if (GSS_Active && ref == *g_thePlayer)
		{
			if (animName->data && BlockAnimGSS(animName->data))
			{
				return false;
			}
		}
	}

	return true;
}


static UInt32 fnIAnimationGraphManagerHolder_Unk_01;
static UInt32 fnIPlayerAnimationGraphManagerHolder_Unk_01;

static void __declspec(naked) Hook_IAnimationGraphManagerHolder_Unk_01(void)
{
	__asm
	{
		push	ecx
		lea		ecx, [esp+4]
		push	ecx
		call	Log_IAnimationGraphManagerHolder_SendEvent
		add		esp, 4
		pop		ecx
		
		test	al, al
		jnz		jmp_default

		retn	4
jmp_default:
		jmp		fnIAnimationGraphManagerHolder_Unk_01
	}
}

static void __declspec(naked) Hook_IPlayerAnimationGraphManagerHolder_Unk_01(void)
{
	__asm
	{
		push	ecx
		lea		ecx, [esp+4]
		push	ecx
		call	Log_IAnimationGraphManagerHolder_SendEvent
		add		esp, 4
		pop		ecx
		
		test	al, al
		jnz		jmp_default

		retn	4
jmp_default:
		jmp		fnIPlayerAnimationGraphManagerHolder_Unk_01
	}
}


namespace PapyrusEvent
{
	void Init(void)
	{
		UInt32* vptr;

		vptr = (UInt32*)ADDR_IAnimationGraphManagerHolder_VPTR;
		fnIAnimationGraphManagerHolder_Unk_01 = vptr[0x01];
		SafeWrite32((UInt32)&vptr[0x01], (UInt32)&Hook_IAnimationGraphManagerHolder_Unk_01);

		vptr = (UInt32*)ADDR_IPlayerAnimationGraphManagerHolder_VPTR;
		fnIPlayerAnimationGraphManagerHolder_Unk_01 = vptr[0x01];
		SafeWrite32((UInt32)&vptr[0x01], (UInt32)&Hook_IAnimationGraphManagerHolder_Unk_01);
	}
}
